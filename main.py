import phonenumbers
from phonenumbers import geocoder
from phonenumbers import carrier
from opencage.geocoder import OpenCageGeocode
import folium


# Find country phone
num = "COUNTRY_CODE + YOUR PHONE"
monNum = phonenumbers.parse(num)
localisation = geocoder.description_for_number(monNum, "fr")
print(localisation)


# Find the mobile operator
operateur = phonenumbers.parse(num)
print(carrier.name_for_number(operateur, "fr"))


# Finding latitude and longitude
key = "YOUR_KEY_OPEN_CAGE"
coord = OpenCageGeocode(key)
request = str(localisation)
response = coord.geocode(request)
print(response)

lat = response[0]["geometry"]["lat"]
lng = response[0]["geometry"]["lng"]

print(lat, lng)


# Creation of the map
maMap = folium.Map(location=[lat, lng], zoom_start=12)
folium.Marker([lat, lng], popup=localisation).add_to(maMap)
maMap.save("map.html")
